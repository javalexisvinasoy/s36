// Express Server Setup

const express = require('express');
const mongoose = require('mongoose');

// This allows us to use all the routes defined in the "taskRoute.js"
const taskRoutes = require('./routes/taskRoutes.js')

// Express Setup
const app = express();
const port = 3001;

app.use(express.json());
app.use(express.urlencoded({extended: true}));
// Routes
app.use('/tasks', taskRoutes)
// Mongoose Setup
// update the <password> and after .net/ add the name of DB "S36"
mongoose.connect(`mongodb+srv://alexisvnsy:admin123@zuittbatch197.8oalahx.mongodb.net/S36?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection
db.on('error', console.error.bind(console, "Connection Error"));
db.once('open', () => console.log('Connection to MongoDB!'))


// NOTE: Make sure to assign a specific endpoint for every database collection. In this case, since we are manipulating the 'tasks' collection in MongoDb, then we are specifying an endpoint with specific routes and controllers for that collection.

app.listen(port, () => console.log(`Server is running at port ${port}`))